# README #

### Quick summary ###

Published in Play Store - https://play.google.com/store/apps/details?id=com.hally.sortingvisualizer.app

Using this app you can easily get familiar with most popular sorting algorithms through a step by step animation with explanatory text. It is possible to pause and resume current playing sorting animation, play it again from the beginning or simply generate a new sequence to sort. Supports English and Russian languages.

### Screenshots, Device - Nexus 5, Android version 5.1.1 ###

![1 Nexus5 screen HeapSort_small.png](https://bitbucket.org/repo/5Gpz6j/images/1449765943-1%20Nexus5%20screen%20HeapSort_small.png)

![2 Nexus5 HowToUse - en.png](https://bitbucket.org/repo/5Gpz6j/images/1976187040-2%20Nexus5%20HowToUse%20-%20en.png)

![2 Nexus5 screen HeapSort_small.png](https://bitbucket.org/repo/5Gpz6j/images/1062371750-2%20Nexus5%20screen%20HeapSort_small.png)

![3 Screenshot_2015-07-27-14-04-13.png](https://bitbucket.org/repo/5Gpz6j/images/682927245-3%20Screenshot_2015-07-27-14-04-13.png)

![4 Screenshot_2015-07-27-14-04-21.png](https://bitbucket.org/repo/5Gpz6j/images/1986891395-4%20Screenshot_2015-07-27-14-04-21.png)

![5 Screenshot_2015-07-27-14-04-33.png](https://bitbucket.org/repo/5Gpz6j/images/2725028925-5%20Screenshot_2015-07-27-14-04-33.png)

![6 Screenshot_2015-07-27-14-11-28.png](https://bitbucket.org/repo/5Gpz6j/images/1986764879-6%20Screenshot_2015-07-27-14-11-28.png)

**Russian localization**

![1 Screenshot_2015-07-27-14-58-06_ru.png](https://bitbucket.org/repo/5Gpz6j/images/2473505561-1%20Screenshot_2015-07-27-14-58-06_ru.png)

![2 Nexus5 HowToUse - ru.png](https://bitbucket.org/repo/5Gpz6j/images/2446958736-2%20Nexus5%20HowToUse%20-%20ru.png)

**Mockup**

![Mockup1.jpg](https://bitbucket.org/repo/5Gpz6j/images/2874527632-Mockup1.jpg)