package com.hally.sortingvis.app.algorithm;

import android.test.AndroidTestCase;

import com.hally.sortingvis.app.model.AnimationManager;

/**
 * @author Kateryna Levshova
 * @date 23.04.2015
 */
public class HeapSortTest extends AndroidTestCase
{
	public static final String CLASS_NAME = HeapSortTest.class.getSimpleName();
	private HeapSort _heapSort;

	public void setUp()
	{
		AnimationManager.getInstance(getContext());
		AnimationManager.IS_NORMAL_MODE = false;
		_heapSort = new HeapSort(null);
	}

	public void testSort1()
	{
		int[] sourceArray = {8, 5, 6, 4, 3, 9, 7, 2, 1, 0};
		_heapSort.sort(sourceArray.clone());

		int[] resultArray = _heapSort.getResultArray();
		int[] expectedArray = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

		assertEquals(resultArray.length, expectedArray.length);

		for (int i = 0; i < resultArray.length; i++)
		{
			assertEquals("Element[" + i + "]", resultArray[i], expectedArray[i]);
		}
	}

	public void testSort2()
	{
		int[] sourceArray = {9, 8, 4, 3, 5, 7, 0, 1, 2, 6};
		_heapSort.sort(sourceArray.clone());

		int[] resultArray = _heapSort.getResultArray();
		int[] expectedArray = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

		assertEquals(resultArray.length, expectedArray.length);

		for (int i = 0; i < resultArray.length; i++)
		{
			assertEquals("Element[" + i + "]", resultArray[i], expectedArray[i]);
		}
	}
}
