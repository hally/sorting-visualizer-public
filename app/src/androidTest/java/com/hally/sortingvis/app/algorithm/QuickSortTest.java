package com.hally.sortingvis.app.algorithm;

import android.content.Context;
import android.content.res.Resources;
import android.test.AndroidTestCase;
import android.test.InstrumentationTestCase;
import android.test.ServiceTestCase;

import com.hally.sortingvis.app.model.AnimationManager;

import junit.framework.Assert;

import java.lang.reflect.Method;

/**
 * @author Kateryna Levshova
 * @date 23.04.2015
 */
public class QuickSortTest extends AndroidTestCase
{
	public static final String CLASS_NAME = HeapSortTest.class.getSimpleName();
	private QuickSort _quickSort;

	public void setUp()
	{
		AnimationManager.getInstance(getContext());
		AnimationManager.IS_NORMAL_MODE = false;
		_quickSort = new QuickSort(null);
	}

	public void testSortTwoElements()
	{
		int[] twoElementsArray = {2, 1};
		_quickSort.sort(twoElementsArray);

		int[] expectedArray = {1, 2};

		assertEquals(expectedArray[0], _quickSort.getResultArray()[0]);
		assertEquals(expectedArray[1], _quickSort.getResultArray()[1]);
	}

	public void testExceptionalCase()
	{
		int[] onlyOneElement = {1};

		try
		{
			_quickSort.sort(onlyOneElement);
			Assert.fail("Should have thrown RuntimeException exception that " +
					QuickSort.ERROR_ARRAY_DEFINITION);
		}
		catch (RuntimeException e)
		{
			assertEquals(e.getMessage(), QuickSort.ERROR_ARRAY_DEFINITION);
		}
	}

	public void tesSwapNotEqualNumbers1()
	{
		int[] sourceArray = {40, 10, 15};

		_quickSort.swapNumbers(sourceArray, 0, 2);

		int[] expectedArray = {15, 10, 40};

		assertEquals(expectedArray[0], _quickSort.getResultArray()[0]);
		assertEquals(expectedArray[1], _quickSort.getResultArray()[1]);
		assertEquals(expectedArray[2], _quickSort.getResultArray()[0]);
	}

	public void tesSwapEqualNumbers2()
	{
		int[] sourceArray = {9, 5, 9};

		_quickSort.swapNumbers(sourceArray, 0, 3);

		int[] expectedArray = {9, 5, 9};

		assertEquals(expectedArray[0], _quickSort.getResultArray()[0]);
		assertEquals(expectedArray[1], _quickSort.getResultArray()[1]);
		assertEquals(expectedArray[2], _quickSort.getResultArray()[0]);
	}

	public void testPerform1()
	{
		int[] sourceArray = {8, 5, 6, 4, 3, 9, 7, 2, 1, 0};
		int[] resultArray = _quickSort.perform(sourceArray.clone(), 0, sourceArray.length - 1);

		int[] expectedArray = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

		assertEquals(resultArray.length, expectedArray.length);

		for (int i = 0; i < resultArray.length; i++)
		{
			assertEquals("Element[" + i + "]", resultArray[i], expectedArray[i]);
		}
	}

	public void testPerform2()
	{
		int[] sourceArray = {2, 9, 8, 3, 0, 6, 4, 7, 1, 5};
		int[] resultArray = _quickSort.perform(sourceArray.clone(), 9, 9);

		int[] expectedArray = {2, 9, 8, 3, 0, 6, 4, 7, 1, 5};

		for (int i = 0; i < resultArray.length; i++)
		{
			assertEquals("Element[" + i + "]", resultArray[i], expectedArray[i]);
		}
	}

	public void testPerform3()
	{
		int[] sourceArray = {9, 8, 4, 3, 5, 7, 1, 0, 2, 6};
		int[] resultArray = _quickSort.perform(sourceArray.clone(), 0, 3);

		int[] expectedArray = {3, 4, 8, 9, 5, 7, 1, 0, 2, 6};

		assertEquals(resultArray.length, expectedArray.length);

		for (int i = 0; i < resultArray.length; i++)
		{
			assertEquals("Element[" + i + "]", resultArray[i], expectedArray[i]);
		}
	}

	public void testPerform4()
	{
		int[] sourceArray = {9, 8, 4, 3, 5, 7, 1, 0, 2, 6};
		int[] resultArray = _quickSort.perform(sourceArray.clone(), 0, 6);

		int[] expectedArray = {1, 3, 4, 5, 7, 8, 9, 0, 2, 6};

		assertEquals(resultArray.length, expectedArray.length);

		for (int i = 0; i < resultArray.length; i++)
		{
			assertEquals("Element[" + i + "]", resultArray[i], expectedArray[i]);
		}
	}

	public void testPerform5()
	{
		int[] sourceArray = {9, 8, 4, 3, 5, 7, 1, 0, 2, 6};
		int[] resultArray = _quickSort.perform(sourceArray.clone(), 6, 9);

		int[] expectedArray = {9, 8, 4, 3, 5, 7, 0, 1, 2, 6};

		assertEquals(resultArray.length, expectedArray.length);

		for (int i = 0; i < resultArray.length; i++)
		{
			assertEquals("Element[" + i + "]", resultArray[i], expectedArray[i]);
		}
	}
}
