package com.hally.sortingvis.app.util;

import android.test.AndroidTestCase;

import junit.framework.Assert;

/**
 * @author Kateryna Levshova
 * @date 17.04.2015
 */
public class SizeGeneratorTest extends AndroidTestCase
{
	public static final String CLASS_NAME = SizeGeneratorTest.class.getSimpleName();
	public static final int SORT_ELEMENTS_AMOUNT = 10;
	SizeGenerator _sizeGenerator;

	/**
	 * Checks normal condition</code>
	 *
	 * @throws RuntimeException
	 */
	public void testGetSizeNormalCondition1()
	{
		float minSize = 40;
		float maxSize = 80;

		_sizeGenerator =
				new SizeGenerator(SORT_ELEMENTS_AMOUNT, minSize, maxSize);

		// Expected size for 0 sorting value
		int resultSize = _sizeGenerator.getSize(0);
		int expectedSize = 40;

		assertEquals(resultSize, expectedSize);

		// Expected size for 1 sorting value
		resultSize = _sizeGenerator.getSize(1);
		expectedSize = 44;

		assertEquals(resultSize, expectedSize);
	}

	/**
	 * Checks normal condition</code>
	 *
	 * @throws RuntimeException
	 */
	public void testGetSizeNormalCondition2()
	{
		float minSize = 40;
		float maxSize = 40;

		_sizeGenerator =
				new SizeGenerator(SORT_ELEMENTS_AMOUNT, minSize, maxSize);

		// Expected size for 0 sorting value
		int resultSize = _sizeGenerator.getSize(0);
		int expectedSize = 40;

		assertEquals(resultSize, expectedSize);

		// Expected size for 1 sorting value
		resultSize = _sizeGenerator.getSize(1);
		expectedSize = 40;

		assertEquals(resultSize, expectedSize);
	}


	/**
	 * Checks throwing error if <code>minSize == 0</code>
	 *
	 * @throws RuntimeException
	 */
	public void testGetSizeExceptional1()
	{
		int minSize = 0;
		int maxSize = 80;

		try
		{
			_sizeGenerator =
					new SizeGenerator(SORT_ELEMENTS_AMOUNT, minSize, maxSize);
			Assert.fail("Should have thrown RuntimeException exception that " +
					"" + SizeGenerator.EXCEPTION_ERROR_SIZE_ZERO);
		}
		catch (RuntimeException e)
		{
			assertEquals(SizeGenerator.EXCEPTION_ERROR_SIZE_ZERO, e.getMessage());
		}
	}

	/**
	 * Checks throwing error if <code>maxSize == 0</code>
	 *
	 * @throws RuntimeException
	 */
	public void testGetSizeExceptional2()
	{
		int minSize = 40;
		int maxSize = 0;

		try
		{
			_sizeGenerator =
					new SizeGenerator(SORT_ELEMENTS_AMOUNT, minSize, maxSize);
			Assert.fail("Should have thrown RuntimeException exception that " +
					"" + SizeGenerator.EXCEPTION_ERROR_SIZE_ZERO);
		}
		catch (RuntimeException e)
		{
			assertEquals(SizeGenerator.EXCEPTION_ERROR_SIZE_ZERO, e.getMessage());
		}
	}

	/**
	 * Checks throwing error if <code>minSize == 0 && maxSize == 0</code>
	 *
	 * @throws RuntimeException
	 */
	public void testGetSizeExceptional3()
	{
		int minSize = 0;
		int maxSize = 0;

		try
		{
			_sizeGenerator =
					new SizeGenerator(SORT_ELEMENTS_AMOUNT, minSize, maxSize);
			Assert.fail("Should have thrown RuntimeException exception that " +
					"" + SizeGenerator.EXCEPTION_ERROR_SIZE_ZERO);
		}
		catch (RuntimeException e)
		{
			assertEquals(SizeGenerator.EXCEPTION_ERROR_SIZE_ZERO, e.getMessage());
		}
	}

	/**
	 * Checks throwing error if <code>maxSize < minSize</code>
	 *
	 * @throws RuntimeException
	 */
	public void testGetSizeExceptional4()
	{
		int minSize = 80;
		int maxSize = 40;

		try
		{
			_sizeGenerator =
					new SizeGenerator(SORT_ELEMENTS_AMOUNT, minSize, maxSize);
			Assert.fail("Should have thrown RuntimeException exception that " +
					"" + SizeGenerator.EXCEPTION_ERROR_SIZE_COMP);
		}
		catch (RuntimeException e)
		{
			assertEquals(SizeGenerator.EXCEPTION_ERROR_SIZE_COMP, e.getMessage());
		}
	}

	/**
	 * Checks throwing error if <code>elementsNumber==0</code>
	 *
	 * @throws RuntimeException
	 */
	public void testGetSizeExceptional5()
	{
		int minSize = 40;
		int maxSize = 80;

		try
		{
			_sizeGenerator =
					new SizeGenerator(0, minSize, maxSize);
			Assert.fail("Should have thrown RuntimeException exception that " +
					"" + SizeGenerator.EXCEPTION_ERROR_NUMBER_LENGTH);
		}
		catch (RuntimeException e)
		{
			assertEquals(SizeGenerator.EXCEPTION_ERROR_NUMBER_LENGTH, e.getMessage());
		}
	}

	public void testGetSizeNormalCondition3()
	{
		_sizeGenerator = new SizeGenerator(SizeGenerator.MIN_ELEMENTS_NUMBER, 40, 80);

		int[] sizeArray = {40, 80};

		int resultSize = _sizeGenerator.getSize(0);
		int expectedSize = 40;

		assertEquals(expectedSize, resultSize);

		resultSize = _sizeGenerator.getSize(1);
		expectedSize = 80;
		assertEquals(expectedSize, resultSize);
	}
}


