package com.hally.sortingvis.app.util;

import android.test.AndroidTestCase;

import java.util.Arrays;

/**
 * @author Kateryna Levshova
 * @date 16.04.2015
 */
public class RandomUtilTest extends AndroidTestCase
{
	public static final String CLASS_NAME = RandomUtilTest.class.getSimpleName();
	public static final int SORT_ELEMENTS_AMOUNT_1 = 10;
	public static final int SORT_ELEMENTS_AMOUNT_2 = 20;

	public void setUp()
	{
	}

	public void testInitSourceArray()
	{
		//test Array with length 10
		RandomUtil.initSourceArray(SORT_ELEMENTS_AMOUNT_1);

		assertEquals(TraceUtil.getTraceMessageFormat(CLASS_NAME, "setUp"), SORT_ELEMENTS_AMOUNT_1,
				RandomUtil.getValuesArrayLength());
		RandomUtil.printValuesArray();

		//test Array with length 20
		RandomUtil.initSourceArray(SORT_ELEMENTS_AMOUNT_2);

		assertEquals(TraceUtil.getTraceMessageFormat(CLASS_NAME, "setUp"), SORT_ELEMENTS_AMOUNT_2,
				RandomUtil.getValuesArrayLength());
		RandomUtil.printValuesArray();
	}

	public void testShuffleArray()
	{
		// Test first shuffle of the array
		RandomUtil.initSourceArray(SORT_ELEMENTS_AMOUNT_2);
		RandomUtil.printValuesArray();

		String initialValue = Arrays.toString(RandomUtil.getValuesArray());

		RandomUtil.shuffleArray();
		RandomUtil.printValuesArray();

		String expectedValue = Arrays.toString(RandomUtil.getValuesArray());

		String message = TraceUtil.getTraceMessageFormat(CLASS_NAME, "testShuffleArray")+
				" expectedValue should not be the same as initialValue!"+"\n"+
				"initialValue= "+initialValue + "\n"+
				"expectedValue= "+expectedValue;
		assertFalse(message, initialValue == expectedValue);
	}

	public void testShuffleArray1()
	{
		int elementsNumber = 5;
		RandomUtil.generateRandomValues(elementsNumber);

		for(int i = 0; i < elementsNumber - 1; i++)
		{
			boolean hasRepeatedValue = hasRepeatedElement(i);
			assertTrue("Source array has value "+i+" more than once!", hasRepeatedValue == false);
		}
	}

	private boolean hasRepeatedElement(int testValue)
	{
		int[] resultArray = RandomUtil.getValuesArray();

		int counter = 0;

		for(int i=0; i < resultArray.length; i++)
		{
			if(resultArray[i] == testValue)
			{
				counter++;
			}
		}

		if(counter > 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
