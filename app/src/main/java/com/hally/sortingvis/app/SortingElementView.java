package com.hally.sortingvis.app;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * @author Kateryna Levshova
 * @date 16.04.2015
 */
public class SortingElementView extends RelativeLayout
{
	private TextView _txtNumber;
	private SortingElementBackground _sortingElementBackground;
	private int _sortingValue;

	public SortingElementView(Context context, AttributeSet attrs, int defStyleAttr,
							  int defStyleRes)
	{
		super(context, attrs, defStyleAttr, defStyleRes);
		init();
	}

	public SortingElementView(Context context, AttributeSet attrs, int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
		init();
	}

	public SortingElementView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init();
	}

	public SortingElementView(Context context)
	{
		super(context);
		init();
	}

	private void init()
	{
		LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context
				.LAYOUT_INFLATER_SERVICE);

		RelativeLayout relativeLayout =
				(RelativeLayout) layoutInflater.inflate(R.layout.sorting_element_view,
						this, true);

		_txtNumber = (TextView) relativeLayout.findViewById(R.id.txtNumber);
		_sortingElementBackground = (SortingElementBackground) relativeLayout.findViewById(R.id
				.imageSortingElementBg);

		//setGravity(Gravity.CENTER_HORIZONTAL|Gravity.BOTTOM);
	}

	/**
	 * Gets sorting value
	 *
	 * @return
	 */
	public int getValue()
	{
		return _sortingValue;
	}

	/**
	 * Converts int value to a String and sets to <code>txtNumber<code/>
	 *
	 * @param value
	 */
	public void setValue(int value)
	{
		_sortingValue = value;
		_txtNumber.setText(String.valueOf(value));
	}

	/**
	 * Sets size taking into account layout parameters of background image
	 *
	 * @param size
	 */
	public void setSize(int size)
	{
		int minSize = Math.round(getResources().getDimension(R.dimen.sorting_element_min_size));
		_sortingElementBackground.getLayoutParams().width = minSize;
		_sortingElementBackground.getLayoutParams().height = size;
	}

	/**
	 * Gets background image width taking into account layout parameters
	 *
	 * @return
	 */
	public int getBackgroundWidth()
	{
		return _sortingElementBackground.getLayoutParams().width;
	}

	/**
	 * Sets background image resource
	 *
	 * @param resId
	 */
	public void setImageResource(int resId)
	{
		this._sortingElementBackground.setImageResource(resId);
	}
}
