package com.hally.sortingvis.app.message;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import com.hally.sortingvis.app.util.TraceUtil;

/**
 * Main class in the event model.
 *
 * @author Kateryna Levshova
 * @date 27.04.2015
 */
public class MessageBroadcaster
{
	public static final String CLASS_NAME = MessageBroadcaster.class.getSimpleName();

	public MessageBroadcaster()
	{
	}

	/**
	 * Sends custom message using <code>LocalBroadcastManager</code> with parameters
	 *
	 * @param context
	 * @param id      - custom message id, use constant
	 * @param params  - key, value bundle
	 */
	public static void sendBroadcastBundle(Context context, String id, Bundle params)
	{
		if (context != null)
		{
			Intent intent = new Intent(id);
			intent.putExtras(params);
			LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
		}
		else
		{
			TraceUtil.logD(CLASS_NAME, "sendBroadcastBundle", "Can't send message if context is " +
					"NULL!");
		}
	}

	/**
	 * Sends custom message using <code>LocalBroadcastManager</code>
	 *
	 * @param context
	 * @param id
	 */
	public static void sendBroadcast(Context context, String id)
	{
		if (context != null)
		{
			Intent intent = new Intent(id);
			LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
		}
		else
		{
			TraceUtil.logD(CLASS_NAME, "sendBroadcast", "Can't send message if context is " +
					"NULL!");
		}
	}
}
