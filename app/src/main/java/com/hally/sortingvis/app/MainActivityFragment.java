package com.hally.sortingvis.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hally.sortingvis.app.algorithm.HeapSort;
import com.hally.sortingvis.app.algorithm.QuickSort;
import com.hally.sortingvis.app.message.IReceiver;
import com.hally.sortingvis.app.message.ISender;
import com.hally.sortingvis.app.message.MessageBroadcaster;
import com.hally.sortingvis.app.message.MessageConstants;
import com.hally.sortingvis.app.model.Constants;
import com.hally.sortingvis.app.util.RandomUtil;


/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment implements ISender, IReceiver
{
	public static final String CLASS_NAME = MainActivityFragment.class.getSimpleName();
	private QuickSort _quickSort;
	private HeapSort _heapSort;
	private TextView _txtOutput;
	private Animation _updateTextAnimation;
	private LinearLayout _fragmentLinearLayout;
	private TextWatcher _textWatcher = new TextWatcher()
	{
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after)
		{
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count)
		{
			_updateTextAnimation.reset();
			_txtOutput.startAnimation(_updateTextAnimation);
		}

		@Override
		public void afterTextChanged(Editable s)
		{
		}
	};

	private Animation.AnimationListener _animationListener = new Animation.AnimationListener()
	{
		@Override
		public void onAnimationEnd(Animation animation)
		{
			sendBroadcast(getActivity(), MessageConstants.RELEASE_NEXT_ANIMATION);
		}

		@Override
		public void onAnimationStart(Animation animation)
		{
		}

		@Override
		public void onAnimationRepeat(Animation animation)
		{
		}
	};

	private BroadcastReceiver _onMessageReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			parseMessage(intent.getAction(), intent.getExtras());
		}
	};

	private View.OnClickListener _onContainerClickListener = new View.OnClickListener()
	{
		@Override
		public void onClick(View v)
		{
			sendBroadcast(getActivity(), MessageConstants.ON_CLICK);
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);

		if (_quickSort == null)
		{
			_quickSort = new QuickSort(getActivity());
		}

		if (_heapSort == null)
		{
			_heapSort = new HeapSort(getActivity());
		}

		initUpdateAnimation();
		registerAllMessagesReceiver();
	}

	@Override
	public void onDestroyView()
	{
		super.onDestroyView();
		sendBroadcast(getActivity(), MessageConstants.UNREGISTER_LISTENERS);
		removeEventListeners();
		unregisterAllMessagesReceiver();
	}

	private void removeEventListeners()
	{
		_fragmentLinearLayout.setOnClickListener(null);
		_txtOutput.removeTextChangedListener(_textWatcher);
		_updateTextAnimation.setAnimationListener(null);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		inflater.inflate(R.menu.menu_main_fragment, menu);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.fragment_main, container, false);
		_txtOutput = (TextView) view.findViewById(R.id.txtOutput);
		_txtOutput.addTextChangedListener(_textWatcher);
		_fragmentLinearLayout = (LinearLayout) view.findViewById(R.id.fragmentLinearLayout);
		_fragmentLinearLayout.setOnClickListener(_onContainerClickListener);
		return view;
	}

	private void initUpdateAnimation()
	{
		if (_updateTextAnimation == null)
		{
			_updateTextAnimation = AnimationUtils.loadAnimation(getActivity(),
					R.anim.update_output);
			_updateTextAnimation.setAnimationListener(_animationListener);
		}
		else
		{
			if (_txtOutput != null)
			{
				Animation updateAnimation = _txtOutput.getAnimation();

				if (updateAnimation != null)
				{
					updateAnimation.cancel();
					_txtOutput.clearAnimation();
					_txtOutput.setAnimation(null);
				}
				_txtOutput.removeTextChangedListener(_textWatcher);
				_txtOutput.setText(R.string.txt_init_state);
				_txtOutput.addTextChangedListener(_textWatcher);
			}
		}
	}

	private void resetAll()
	{
		initUpdateAnimation();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		int id = item.getItemId();

		switch (id)
		{
			case R.id.action_quick:
			{
				_quickSort.sort(RandomUtil.getValuesArray().clone());
				break;
			}
			case R.id.action_heap:
				_heapSort.sort(RandomUtil.getValuesArray().clone());
				break;
			case R.id.action_randomize:
			{
				resetAll();
				sendBroadcast(getActivity(), MessageConstants.ON_ACTION_RANDOMIZE);
				break;
			}
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void sendBroadcastBundle(Context context, String id, Bundle params)
	{
		MessageBroadcaster.sendBroadcastBundle(context, id, params);
	}

	@Override
	public void sendBroadcast(Context context, String id)
	{
		MessageBroadcaster.sendBroadcast(context, id);
	}

	@Override
	public void registerAllMessagesReceiver()
	{
		registerOnMessageReceiver(MessageConstants.UPDATE_OUTPUT);
		registerOnMessageReceiver(MessageConstants.RESTART_ANIMATION);
	}

	@Override
	public void unregisterAllMessagesReceiver()
	{
		LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(_onMessageReceiver);
	}

	@Override
	public void registerOnMessageReceiver(String messageId)
	{
		LocalBroadcastManager.getInstance(getActivity()).registerReceiver(_onMessageReceiver,
				new IntentFilter(messageId));
	}

	protected void parseMessage(String messageId, Bundle bundle)
	{
		switch (messageId)
		{
			case MessageConstants.UPDATE_OUTPUT:
			{
				_txtOutput.setText(bundle.getString(Constants.TEXT, ""));
				break;
			}
			case MessageConstants.RESTART_ANIMATION:
			{
				resetAll();
				break;
			}
		}
	}
}
