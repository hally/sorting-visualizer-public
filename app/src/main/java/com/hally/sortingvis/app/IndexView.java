package com.hally.sortingvis.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.hally.sortingvis.app.message.IReceiver;
import com.hally.sortingvis.app.message.MessageConstants;
import com.hally.sortingvis.app.util.TraceUtil;

/**
 * @author Kateryna Levshova
 * @date 27.04.2015
 */
public class IndexView extends ImageView implements IReceiver
{
	public static final String CLASS_NAME = IndexView.class.getSimpleName();
	public static final String LEFT_MARGIN = "LEFT_MARGIN";
	private BroadcastReceiver _onMessageReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			parseMessage(intent.getAction(), intent.getExtras());
		}
	};

	public IndexView(Context context)
	{
		super(context);
		init();
	}

	public IndexView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init();
	}

	public IndexView(Context context, AttributeSet attrs, int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
		init();
	}

	public IndexView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes)
	{
		super(context, attrs, defStyleAttr, defStyleRes);
		init();
	}

	public void init()
	{
		registerAllMessagesReceiver();
	}

	protected void parseMessage(String messageId, Bundle bundle)
	{
		switch (messageId)
		{
			case MessageConstants.UNREGISTER_LISTENERS:
			{
				unregisterAllMessagesReceiver();
				break;
			}
			case MessageConstants.UPDATE_INDEX_POSITION:
			{
				setX(bundle.getFloat(IndexView.LEFT_MARGIN));
				break;
			}
		}
	}

	/**
	 * @return Resource dimension value multiplied by the appropriate  metric.
	 */
	public float getIndexImageSize()
	{
		return getResources().getDimension(R.dimen.index_size);
	}

	/**
	 * Call it in onCreate
	 */
	@Override
	public void registerAllMessagesReceiver()
	{
		registerOnMessageReceiver(MessageConstants.UNREGISTER_LISTENERS);
		registerOnMessageReceiver(MessageConstants.UPDATE_INDEX_POSITION);
	}

	public void registerOnMessageReceiver(String messageId)
	{
		LocalBroadcastManager.getInstance(getContext()).registerReceiver(_onMessageReceiver,
				new IntentFilter(messageId));
	}

	/**
	 * Call it in onDestroy
	 */
	@Override
	public void unregisterAllMessagesReceiver()
	{
		LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(_onMessageReceiver);
	}
}
