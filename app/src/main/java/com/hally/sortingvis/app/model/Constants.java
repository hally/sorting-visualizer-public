package com.hally.sortingvis.app.model;

/**
 * @author Kateryna Levshova
 * @date 01.07.2015
 */
public class Constants
{
	public static final int MOVE_INDEX_DURATION = 500;
	public static final String SORTING_ELEMENT_TAG = "sorting_element";
	public static final String INDEX_FROM = "INDEX_FROM";
	public static final String INDEX_TO = "INDEX_TO";
	public static final String LOWER_INDEX = "LOWER_INDEX";
	public static final String HIGHER_INDEX = "HIGHER_INDEX";
	public static final String INDEX = "INDEX";
	public static final String PIVOT_INDEX = "PIVOT_INDEX";
	public static final String TEXT = "TEXT";
	public static String LEFT_STR = "";
	public static String RIGHT_STR = "";
}
