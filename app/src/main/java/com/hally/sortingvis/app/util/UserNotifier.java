package com.hally.sortingvis.app.util;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.support.v4.app.NotificationCompat;

/**
 * @author Kateryna Levshova
 * @date 27.04.2015
 */
public class UserNotifier
{
	private static final int VIBRATE_NOTIFICATION_ID = 3004;
	private static final int SOUND_NOTIFICATION_ID = 3005;

	public static void vibrate(Context context)
	{
		NotificationManager notificationManager = (NotificationManager)
				context.getSystemService(Context.NOTIFICATION_SERVICE);

		NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context);

		Notification note = notificationBuilder.build();
		note.defaults |= Notification.DEFAULT_VIBRATE;

		notificationManager.notify(VIBRATE_NOTIFICATION_ID, note);
	}

	public static void sound(Context context)
	{
		NotificationManager notificationManager = (NotificationManager)
				context.getSystemService(Context.NOTIFICATION_SERVICE);

		NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context);

		Notification note = notificationBuilder.build();
		note.defaults |= Notification.DEFAULT_SOUND;

		notificationManager.notify(SOUND_NOTIFICATION_ID, note);
	}
}
