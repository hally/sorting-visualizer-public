package com.hally.sortingvis.app;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.hally.sortingvis.app.message.ISender;
import com.hally.sortingvis.app.message.MessageBroadcaster;
import com.hally.sortingvis.app.message.MessageConstants;
import com.hally.sortingvis.app.model.Constants;
import com.hally.sortingvis.app.util.RandomUtil;
import com.hally.sortingvis.app.util.TraceUtil;

/**
 * @author Kateryna Levshova
 * @date 28.04.2015
 */
public class AnimatedNumbersContainer extends NumbersContainer
{
	public static final String CLASS_NAME = AnimatedNumbersContainer.class.getSimpleName();
	private ObjectAnimator _swapLowerAnimator;
	private ObjectAnimator _swapHigherAnimator;
	private Animation _markPivotAnimation;
	private SortingElementView _currentPivot;

	private Animation.AnimationListener _animationListener = new Animation.AnimationListener()
	{
		@Override
		public void onAnimationEnd(Animation animation)
		{
			sendBroadcast(getContext(), MessageConstants.RELEASE_NEXT_ANIMATION);
		}

		@Override
		public void onAnimationStart(Animation animation)
		{
		}

		@Override
		public void onAnimationRepeat(Animation animation)
		{
		}
	};

	public AnimatedNumbersContainer(Context context)
	{
		super(context);
		initAnimations();
	}

	public AnimatedNumbersContainer(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		initAnimations();
	}

	public AnimatedNumbersContainer(Context context, AttributeSet attrs, int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
		initAnimations();
	}

	public AnimatedNumbersContainer(Context context, AttributeSet attrs, int defStyleAttr,
									int defStyleRes)
	{
		super(context, attrs, defStyleAttr, defStyleRes);
		initAnimations();
	}

	@Override
	public void registerDifferentReceiver()
	{
		registerOnMessageReceiver(MessageConstants.MARK_PIVOT);
		registerOnMessageReceiver(MessageConstants.SWAP_ELEMENTS);
		registerOnMessageReceiver(MessageConstants.RESTART_ANIMATION);
	}

	@Override
	protected void initSortingElementsArray()
	{
		RandomUtil.getValuesArray().clone();
	}

	@Override
	protected void onActionRandomize()
	{
		resetAll();
	}

	private void resetAll()
	{
		initAnimations();

		RandomUtil.getValuesArray().clone();
		lineChildrenInCurrentSortingStateOrder();
		resetView();
	}

	private void initAnimations()
	{
		if (_swapLowerAnimator == null)
		{
			_swapLowerAnimator = (ObjectAnimator) AnimatorInflater.loadAnimator(getContext(),
					R.animator.swap_elements);
		}
		else
		{
			if (_swapLowerAnimator.isRunning())
			{
				_swapLowerAnimator.end();
			}
		}

		if (_swapHigherAnimator == null)
		{
			_swapHigherAnimator = (ObjectAnimator) AnimatorInflater.loadAnimator(getContext(),
					R.animator.swap_elements);
		}
		else
		{
			if (_swapHigherAnimator.isRunning())
			{
				_swapHigherAnimator.end();
			}
		}

		initMarkPivotAnimation();
	}

	private void initMarkPivotAnimation()
	{
		if (_markPivotAnimation == null)
		{
			_markPivotAnimation = AnimationUtils.loadAnimation(getContext(),
					R.anim.mark_pivot);

			_markPivotAnimation.setAnimationListener(_animationListener);
		}
		else
		{
			if (_currentPivot != null)
			{
				Animation pivotAnimation = _currentPivot.getAnimation();

				if (pivotAnimation != null)
				{
					pivotAnimation.cancel();
					_currentPivot.clearAnimation();
					_currentPivot.setAnimation(null);
				}
			}
		}
	}

	private void resetView()
	{
		int elementsCount = getChildCount();

		for (int i = 0; i < elementsCount; i++)
		{
			SortingElementView sortingElementView = (SortingElementView) getChildAt(i);
			sortingElementView
					.setImageResource(R.drawable.gradient_element);    //reset default image
		}
	}

	@Override
	protected void createSortingElements()
	{
		super.createSortingElements();

		sendUpdateIndexPositionMessage();
		setVerticalGap();
	}

	/**
	 * Sends UPDATE_INDEX_POSITION message with LEFT_MARGIN parameter
	 */
	private void sendUpdateIndexPositionMessage()
	{
		Bundle bundle = new Bundle();
		// get first element width
		bundle.putFloat(IndexView.LEFT_MARGIN, getSortingElementViewWidth(0) / 2);
		sendBroadcastBundle(getContext(), MessageConstants.UPDATE_INDEX_POSITION, bundle);
	}

	/**
	 * Gets width of sorting element view background
	 *
	 * @param position
	 * @return
	 */
	public int getSortingElementViewWidth(int position)
	{
		return getSortingElementView(position).getBackgroundWidth();
	}

	public float getSortingElementPositionX(int position)
	{
		return getSortingElementView(position).getX();
	}

	/**
	 * Moves container under index element
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setVerticalGap()
	{
		float topGap = getResources().getDimension(R.dimen.index_size);
		setY(topGap);
	}

	protected void lineChildrenInCurrentSortingStateOrder()
	{
		super.lineChildrenInCurrentSortingStateOrder();
		sendUpdateIndexPositionMessage();
	}

	@Override
	protected void parseMessage(String messageId, Bundle bundle)
	{
		super.parseMessage(messageId, bundle);
		switch (messageId)
		{
			case MessageConstants.MARK_PIVOT:
			{
				markPivotElement(bundle.getInt(Constants.PIVOT_INDEX));
				break;
			}
			case MessageConstants.SWAP_ELEMENTS:
			{
				swapElements(bundle.getInt(Constants.LOWER_INDEX), bundle.getInt(
						Constants.HIGHER_INDEX));
				break;
			}
			case MessageConstants.RESTART_ANIMATION:
			{
				resetAll();
				break;
			}
		}
	}

	@Override
	protected void removeEventListeners()
	{
		super.removeEventListeners();
		_swapHigherAnimator.removeAllListeners();
		_markPivotAnimation.setAnimationListener(null);
	}

	/**
	 * Plays <code>swap_elements</code> property animation. Releases the animation queue when
	 * animation ends.
	 *
	 * @param lowerIndex
	 * @param higherIndex
	 */
	private void swapElements(final int lowerIndex, final int higherIndex)
	{
		final SortingElementView lowerElement = getSortingElementView(lowerIndex);
		final SortingElementView higherElement = getSortingElementView(higherIndex);

		float shift = (higherElement.getBackgroundWidth() - lowerElement.getBackgroundWidth()) / 2;

		final float endLowerPositionX = higherElement.getX() + shift;
		final float endHigherPositionX = lowerElement.getX() - shift;

		_swapLowerAnimator.setTarget(lowerElement);
		_swapLowerAnimator.setFloatValues(lowerElement.getX(), endLowerPositionX);
		_swapLowerAnimator.start();

		//--------------
		_swapHigherAnimator.removeAllListeners();
		_swapHigherAnimator.setTarget(higherElement);
		_swapHigherAnimator.setFloatValues(higherElement.getX(), endHigherPositionX);
		_swapHigherAnimator.start();

		_swapHigherAnimator.addListener(new Animator.AnimatorListener()
		{
			@Override
			public void onAnimationStart(Animator animation)
			{
			}

			@Override
			public void onAnimationEnd(Animator animation)
			{

				lowerElement.setTag(Constants.SORTING_ELEMENT_TAG + higherIndex);
				higherElement.setTag(Constants.SORTING_ELEMENT_TAG + lowerIndex);
				sendBroadcast(getContext(), MessageConstants.RELEASE_NEXT_ANIMATION);
			}

			@Override
			public void onAnimationCancel(Animator animation)
			{
			}

			@Override
			public void onAnimationRepeat(Animator animation)
			{
			}
		});
	}

	/**
	 * Changes background image of the pivot element and plays <code>mark_pivot</code> view
	 * animation. Releases the animation queue when animation ends.
	 *
	 * @param pivotIndex
	 */
	private void markPivotElement(int pivotIndex)
	{
		if (_currentPivot != null)
		{
			_currentPivot.setImageResource(R.drawable.gradient_element); // reset previous pivot
		}

		SortingElementView sortingElementView = getSortingElementView(pivotIndex);
		_currentPivot = sortingElementView;
		sortingElementView.setImageResource(R.drawable.gradient_pivot);

		_markPivotAnimation.reset();
		sortingElementView.startAnimation(_markPivotAnimation);
	}
}
