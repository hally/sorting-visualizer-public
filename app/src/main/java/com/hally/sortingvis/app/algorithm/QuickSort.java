package com.hally.sortingvis.app.algorithm;

import android.content.Context;

import com.hally.sortingvis.app.R;
import com.hally.sortingvis.app.model.AnimationManager;
import com.hally.sortingvis.app.model.Constants;
import com.hally.sortingvis.app.util.TraceUtil;

import java.util.Arrays;

/**
 * @author Kateryna Levshova
 * @date 21.04.2015
 */
public class QuickSort extends AbstractSort
{
	public static final String CLASS_NAME = QuickSort.class.getSimpleName();

	public QuickSort(Context context)
	{
		super(context);
	}

	@Override
	public void sort(int[] srcArray) throws RuntimeException
	{
		super.sort(srcArray);
		TraceUtil.logD(CLASS_NAME, "-------SORTING RESULT = ", Arrays.toString(resultArray));
	}

	@Override
	protected int[] perform(int[] srcArray, int start, int end)
	{
		TraceUtil.logD(CLASS_NAME, "perform", Arrays.toString(srcArray));
		TraceUtil.logD(CLASS_NAME, "perform", "start= " + start + ", " + "end= " + end);

		int lowerIndex = start;   // index of left-to-right scan
		int higherIndex = end;    // index of right-to-left scan

		// check that there are at least two elements to sort
		animationManager.updateOutputText(R.string.txt_check_elements_number);

		if (end - start >= 1)
		{
			TraceUtil.logD(CLASS_NAME, "perform", "srcArray[start]= " + srcArray[start] + ", " +
					"srcArray[end]= " + srcArray[end]);
			//mark pivot
			int pivot = srcArray[start];
			animationManager.updateOutputTextWithFormat(R.string.txt_mark_pivot, pivot, start);
			animationManager.updateSortingElementUI(start);
			TraceUtil.logD(CLASS_NAME, "perform", "pivot=" + pivot);

			// while the scan indices from left and right have not met
			animationManager.updateOutputText(R.string.txt_scanning_indexes);

			while (higherIndex > lowerIndex)
			{
				//mark lower and higher indexes
				animationManager.markIndexes(R.string.txt_mark_lo_hi_indexes, lowerIndex,
						higherIndex);

				// from the left, look for the first element greater than the pivot
				while (srcArray[lowerIndex] <= pivot && lowerIndex <= end &&
						higherIndex > lowerIndex)
				{
					lowerIndex++;
					animationManager.updateOutputTextWithFormat(R.string.txt_move_indexes_from_side, Constants.LEFT_STR);
					//moveIndex animation from (lower-1) to lower
					animationManager.startMoveIndexAnimation(lowerIndex - 1, lowerIndex, true);
				}
				// from the right, look for the first element greater than the pivot
				while (srcArray[higherIndex] > pivot && higherIndex >= start &&
						higherIndex >= lowerIndex)
				{
					higherIndex--;
					animationManager.updateOutputTextWithFormat(R.string.txt_move_indexes_from_side,
							Constants.RIGHT_STR);
					//moveIndex animation from (higher+1) to higher
					animationManager.startMoveIndexAnimation(higherIndex + 1, higherIndex, false);
				}
				// if the left seek index is still smaller than right
				if (higherIndex > lowerIndex)
				{
					TraceUtil.logD(CLASS_NAME, "perform", "++++++++++++++");
					animationManager.updateOutputText(R.string.txt_le_smaller_ri);
					// swap the corresponding elements
					swapNumbers(srcArray, lowerIndex, higherIndex);
				}
			}

			// after the indices have crossed, swap the last element in the left partition with
			// the pivot index
			TraceUtil.logD(CLASS_NAME, "perform", "============");
			animationManager.updateOutputText(R.string.txt_indexes_crossed);
			swapNumbers(srcArray, start, higherIndex);

			animationManager.updateOutputTextWithFormat(R.string.txt_quicksort, Constants.LEFT_STR);
			perform(srcArray, start, higherIndex - 1); // quicksort the left partition

			animationManager.updateOutputTextWithFormat(R.string.txt_quicksort, Constants.RIGHT_STR);
			perform(srcArray, higherIndex + 1, end);   // quicksort the right partition
		}
		else    // if there is only one element in the partition, do not do any sorting
		{
			animationManager.updateOutputText(R.string.txt_do_nothing);
			TraceUtil.logD(CLASS_NAME, "perform", "2 do not nothing");
			return srcArray; // the array is sorted, so exit
		}

		animationManager.updateOutputText(R.string.txt_partition_finished);
		return srcArray;
	}
}
