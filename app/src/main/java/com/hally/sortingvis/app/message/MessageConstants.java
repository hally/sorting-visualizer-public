package com.hally.sortingvis.app.message;

/**
 * @author Kateryna Levshova
 * @date 27.04.2015
 */
public class MessageConstants
{
	public static final String UNREGISTER_LISTENERS = "UNREGISTER_LISTENERS";
	public static final String UPDATE_INDEX_POSITION = "UPDATE_INDEX_POSITION";
	public static final String ON_ACTION_RANDOMIZE = "ON_ACTION_RANDOMIZE";

	// Related to animation
	public static final String MOVE_INDEX = "MOVE_INDEX";
	public static final String ANIMATION_SET_FINISHED = "ANIMATION_SET_FINISHED";
	public static final String MARK_PIVOT = "MARK_PIVOT";
	public static final String MARK_INDEXES = "MARK_INDEXES";
	public static final String MARK_INDEX = "MARK_INDEX";
	public static final String SWAP_ELEMENTS = "SWAP_ELEMENTS";

	public static final String RELEASE_NEXT_ANIMATION = "RELEASE_NEXT_ANIMATION";
	public static final String RESTART_ANIMATION = "RESTART_ANIMATION";

	public static final String UPDATE_OUTPUT = "UPDATE_OUTPUT";
	public static final String ON_CLICK = "ON_CLICK";
}
