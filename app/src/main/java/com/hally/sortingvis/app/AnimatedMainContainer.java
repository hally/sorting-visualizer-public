package com.hally.sortingvis.app;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;

import com.hally.sortingvis.app.message.IReceiver;
import com.hally.sortingvis.app.message.ISender;
import com.hally.sortingvis.app.message.MessageBroadcaster;
import com.hally.sortingvis.app.message.MessageConstants;
import com.hally.sortingvis.app.model.AnimationManager;
import com.hally.sortingvis.app.model.Constants;
import com.hally.sortingvis.app.util.TraceUtil;
import com.hally.sortingvis.app.util.UserNotifier;

/**
 * @author Kateryna Levshova
 * @date 27.04.2015
 */
public class AnimatedMainContainer extends RelativeLayout implements IReceiver, ISender
{
	public static final String CLASS_NAME = AnimatedMainContainer.class.getSimpleName();
	private IndexView _indexViewLower;
	private IndexView _indexViewHigher;
	private AnimatedNumbersContainer _animatedNumbersContainer;
	private ObjectAnimator _moveIndexAnimator;
	private Animation _markLowerAnimation;
	private Animation _markHigherAnimation;
	private Animator.AnimatorListener _animatorListener = new Animator.AnimatorListener()
	{
		@Override
		public void onAnimationStart(Animator animation)
		{
		}

		@Override
		public void onAnimationEnd(Animator animation)
		{
			sendBroadcast(getContext(), MessageConstants.RELEASE_NEXT_ANIMATION);
		}

		@Override
		public void onAnimationCancel(Animator animation)
		{
		}

		@Override
		public void onAnimationRepeat(Animator animation)
		{
		}
	};

	private  Animation.AnimationListener _animationListener= new Animation.AnimationListener()
	{
		@Override
			public void onAnimationEnd(Animation animation)
			{
				sendBroadcast(getContext(), MessageConstants.RELEASE_NEXT_ANIMATION);
			}

			@Override
			public void onAnimationStart(Animation animation)
			{
			}

			@Override
			public void onAnimationRepeat(Animation animation)
			{
			}
	};


	private BroadcastReceiver _onMessageReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			parseMessage(intent.getAction(), intent.getExtras());
		}
	};

	public AnimatedMainContainer(Context context, AttributeSet attrs, int defStyleAttr,
								 int defStyleRes)
	{
		super(context, attrs, defStyleAttr, defStyleRes);
		init();
	}

	public AnimatedMainContainer(Context context, AttributeSet attrs, int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
		init();
	}

	public AnimatedMainContainer(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init();
	}

	public AnimatedMainContainer(Context context)
	{
		super(context);
		init();
	}

	public void init()
	{
		if (!isInEditMode())
		{
			LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context
					.LAYOUT_INFLATER_SERVICE);
			layoutInflater.inflate(R.layout.animated_main_container, this, true);

			_animatedNumbersContainer =
					(AnimatedNumbersContainer) findViewById(R.id.animatedNumbersContainer);

			_indexViewLower = (IndexView) findViewById(R.id.indexViewLower);
			_indexViewHigher = (IndexView) findViewById(R.id.indexViewHigher);

			resetAll();
			registerAllMessagesReceiver();
		}
	}

	private void resetAll()
	{
		if (_moveIndexAnimator == null)
		{
			initMoveIndexAnimator();
		}
		else
		{
			if (_moveIndexAnimator.isRunning())
			{
				_moveIndexAnimator.end();
			}
		}

		if (_markLowerAnimation == null)
		{
			_markLowerAnimation = AnimationUtils.loadAnimation(getContext(),
					R.anim.mark_index);
		}

		if (_markHigherAnimation == null)
		{
			initMarkHigherAnimation();
		}

		initIndexesPosition();
	}

	private void initIndexesPosition()
	{
		float positionX = calculatePositionX(0); // for the 1st element
		_indexViewLower.setX(positionX);
		_indexViewHigher.setX(positionX);
	}

	private void initMoveIndexAnimator()
	{
		_moveIndexAnimator = ObjectAnimator.ofFloat(_indexViewLower, X, 0, 0);
		_moveIndexAnimator.setDuration(Constants.MOVE_INDEX_DURATION);

		_moveIndexAnimator.addListener(_animatorListener);
	}

	private void initMarkHigherAnimation()
	{
		_markHigherAnimation = AnimationUtils.loadAnimation(getContext(),
				R.anim.mark_index);
		_markHigherAnimation.setAnimationListener(_animationListener);
	}

	/**
	 * Call it in onCreate
	 */
	@Override
	public void registerAllMessagesReceiver()
	{
		registerOnMessageReceiver(MessageConstants.MOVE_INDEX);
		registerOnMessageReceiver(MessageConstants.MARK_INDEXES);
		registerOnMessageReceiver(MessageConstants.MARK_INDEX);
		registerOnMessageReceiver(MessageConstants.UNREGISTER_LISTENERS);
		registerOnMessageReceiver(MessageConstants.ON_ACTION_RANDOMIZE);
		registerOnMessageReceiver(MessageConstants.ANIMATION_SET_FINISHED);
		registerOnMessageReceiver(MessageConstants.RESTART_ANIMATION);
	}

	public void registerOnMessageReceiver(String messageId)
	{
		LocalBroadcastManager.getInstance(getContext()).registerReceiver(_onMessageReceiver,
				new IntentFilter(messageId));
	}

	private void removeEventListeners()
	{
		_moveIndexAnimator.removeAllListeners();
		_markHigherAnimation.setAnimationListener(null);
	}

	@Override
	public void unregisterAllMessagesReceiver()
	{
		LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(_onMessageReceiver);
	}

	protected void parseMessage(String messageId, Bundle bundle)
	{
		switch (messageId)
		{
			case MessageConstants.UNREGISTER_LISTENERS:
			{
				unregisterAllMessagesReceiver();
				removeEventListeners();
				break;
			}
			case MessageConstants.MOVE_INDEX:
			{
				moveIndex(bundle.getInt(Constants.INDEX_FROM), bundle.getInt(
						Constants.INDEX_TO));
				break;
			}
			case MessageConstants.MARK_INDEXES:
			{
				markIndexes(bundle.getInt(Constants.LOWER_INDEX), bundle.getInt(
						Constants.HIGHER_INDEX));
				break;
			}
			case MessageConstants.MARK_INDEX:
			{
				markIndex(bundle.getInt(Constants.INDEX));
				break;
			}
			case MessageConstants.ON_ACTION_RANDOMIZE:
			case MessageConstants.RESTART_ANIMATION:
			{
				resetAll();
				break;
			}
			case MessageConstants.ANIMATION_SET_FINISHED:
			{
				UserNotifier.sound(getContext());
				break;
			}
		}
	}

	private void markIndex(int index)
	{
		float posX = calculatePositionX(index);
		_indexViewLower.setX(posX);
		_indexViewHigher.startAnimation(_markHigherAnimation);
	}

	/**
	 * Plays <code>mark_index</code> view animation for lower and higher indexes. Releases the
	 * animation queue when animation for higher index ends.
	 *
	 * @param lowerIndex
	 * @param higherIndex
	 */
	private void markIndexes(int lowerIndex, int higherIndex)
	{
		float lowerX = calculatePositionX(lowerIndex);
		_indexViewLower.setX(lowerX);

		float higherX = calculatePositionX(higherIndex);
		_indexViewHigher.setX(higherX);

		_indexViewLower.startAnimation(_markLowerAnimation);
		_indexViewHigher.startAnimation(_markHigherAnimation);
	}

	/**
	 * Plays MOVE_INDEX property animation for lower and higher indexes using
	 * <code>ObjectAnimator</code> Releases animation queue when animation ends.
	 *
	 * @param indexFrom
	 * @param indexTo
	 */
	protected void moveIndex(int indexFrom, int indexTo)
	{
		float startX = calculatePositionX(indexFrom);
		float endX = calculatePositionX(indexTo);

		if (indexFrom < indexTo)
		{
			_moveIndexAnimator.setTarget(_indexViewLower);
		}
		else
		{
			_moveIndexAnimator.setTarget(_indexViewHigher);
		}

		_moveIndexAnimator.setFloatValues(startX, endX);
		_moveIndexAnimator.start();
	}

	/**
	 * Calculates X position for MOVE_INDEX animation. X is a center of sorting element.
	 *
	 * @param elementIndex
	 * @return
	 */
	public float calculatePositionX(int elementIndex)
	{
		float elementX = _animatedNumbersContainer.getSortingElementPositionX(elementIndex);
		float elementWidth = _animatedNumbersContainer.getSortingElementViewWidth(elementIndex);
		return elementX + elementWidth / 2;
	}

	@Override
	public void sendBroadcastBundle(Context context, String id, Bundle params)
	{
		MessageBroadcaster.sendBroadcastBundle(context, id, params);
	}

	@Override
	public void sendBroadcast(Context context, String id)
	{
		MessageBroadcaster.sendBroadcast(context, id);
	}
}
