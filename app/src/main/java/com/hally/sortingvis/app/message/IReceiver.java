package com.hally.sortingvis.app.message;

/**
 * @author Kateryna Levshova
 * @date 27.04.2015
 */
public interface IReceiver
{
	/**
	 * Call it in onCreate
	 */
	public void registerAllMessagesReceiver();

	/**
	 * Call it in onDestroy
	 */
	public void unregisterAllMessagesReceiver();

	/**
	 * Generally called inside of <code>registerAllMessagesReceiver()</code>
	 */
	public void registerOnMessageReceiver(String messageId);
}
