package com.hally.sortingvis.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.hally.sortingvis.app.message.IReceiver;
import com.hally.sortingvis.app.message.ISender;
import com.hally.sortingvis.app.message.MessageBroadcaster;
import com.hally.sortingvis.app.message.MessageConstants;
import com.hally.sortingvis.app.model.Constants;
import com.hally.sortingvis.app.util.RandomUtil;
import com.hally.sortingvis.app.util.SizeGenerator;
import com.hally.sortingvis.app.util.UserNotifier;

/**
 * @author Kateryna Levshova
 * @date 16.04.2015
 */
public class NumbersContainer extends LinearLayout implements IReceiver, ISender
{
	public static final String CLASS_NAME = NumbersContainer.class.getSimpleName();
	public static final int SORTING_ELEMENTS_NUMBER = 10;
	SizeGenerator _sizeGenerator = null;

	private BroadcastReceiver _onMessageReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			parseMessage(intent.getAction(), intent.getExtras());
		}
	};

	public NumbersContainer(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes)
	{
		super(context, attrs, defStyleAttr, defStyleRes);
		init();
	}

	public NumbersContainer(Context context, AttributeSet attrs, int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
		init();
	}

	public NumbersContainer(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init();
	}

	public NumbersContainer(Context context)
	{
		super(context);

		init();
	}

	protected void init()
	{
		if (!isInEditMode())
		{
			LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context
					.LAYOUT_INFLATER_SERVICE);
			layoutInflater.inflate(R.layout.numbers_container, this, true);

			registerAllMessagesReceiver();
			createSortingElements();
			setWeightSum(SORTING_ELEMENTS_NUMBER);
		}
	}

	/**
	 * Changes sorting value and size for all children of <code>NumbersContainer</code>
	 */
	private void shuffleElements()
	{
		RandomUtil.shuffleArray();
		lineChildrenInCurrentSortingStateOrder();
	}

	protected void lineChildrenInCurrentSortingStateOrder()
	{
		int elementsCount = getChildCount();

		for (int i = 0; i < elementsCount; i++)
		{
			setUpSortingElementView(getSortingElementView(i), i);
		}
	}

	/**
	 * Creates sorting elements <code>SortingElementView</> instances and sets size accordingly to
	 * sorting value weight.
	 */
	protected void createSortingElements()
	{
		initSortingElementsArray();

		float minSize = getResources().getDimension(R.dimen.sorting_element_min_size);
		float maxSize = getResources().getDimension(R.dimen.sorting_element_max_size);

		try
		{
			_sizeGenerator = new SizeGenerator(SORTING_ELEMENTS_NUMBER, minSize, maxSize);
		}
		catch (RuntimeException e)
		{
			e.printStackTrace();
		}


		for (int i = 0; i < SORTING_ELEMENTS_NUMBER; i++)
		{
			SortingElementView sortingElementView = new SortingElementView(getContext());
			setUpSortingElementView(sortingElementView, i);
			this.addView(sortingElementView);
		}
	}

	protected void initSortingElementsArray()
	{
		RandomUtil.generateRandomValues(SORTING_ELEMENTS_NUMBER);
	}

	/**
	 * Sets random value and size to defined sorting view element
	 *
	 * @param sortingElementView
	 * @param index
	 */
	private void setUpSortingElementView(SortingElementView sortingElementView, int index)
	{
		int sortingValue = RandomUtil.getValuesArray()[index];

		sortingElementView.setValue(sortingValue);
		sortingElementView.setTag(Constants.SORTING_ELEMENT_TAG + index);

		int size = _sizeGenerator.getSize(sortingValue);
		sortingElementView.setSize(size);

		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout
				.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, 1f);

		sortingElementView.setLayoutParams(lp);
	}

	/**
	 * Gets sorting element view instance by tag <code>Constants.SORTING_ELEMENT_TAG +
	 * position</code>
	 *
	 * @param position
	 * @return
	 */
	public SortingElementView getSortingElementView(int position)
	{
		return (SortingElementView) findViewWithTag(Constants.SORTING_ELEMENT_TAG + position);
	}

	/**
	 * Call it in onCreate
	 */
	@Override
	public void registerAllMessagesReceiver()
	{
		registerOnMessageReceiver(MessageConstants.UNREGISTER_LISTENERS);
		registerOnMessageReceiver(MessageConstants.ON_ACTION_RANDOMIZE);
		registerDifferentReceiver();
	}

	protected void registerDifferentReceiver()
	{
	}

	public void registerOnMessageReceiver(String messageId)
	{
		LocalBroadcastManager.getInstance(getContext()).registerReceiver(_onMessageReceiver,
				new IntentFilter(messageId));
	}

	protected void parseMessage(String messageId, Bundle bundle)
	{
		switch (messageId)
		{
			case MessageConstants.ON_ACTION_RANDOMIZE:
			{
				onActionRandomize();
				break;
			}
			case MessageConstants.UNREGISTER_LISTENERS:
			{
				removeEventListeners();
				unregisterAllMessagesReceiver();
				break;
			}
		}
	}

	protected void onActionRandomize()
	{
		shuffleElements();
		UserNotifier.vibrate(getContext());
	}

	protected void removeEventListeners()
	{
	}

	/**
	 * Call it in onDestroy
	 */
	@Override
	public void unregisterAllMessagesReceiver()
	{
		LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(_onMessageReceiver);
	}

	@Override
	public void sendBroadcastBundle(Context context, String id, Bundle params)
	{
		MessageBroadcaster.sendBroadcastBundle(context, id, params);
	}

	@Override
	public void sendBroadcast(Context context, String id)
	{
		MessageBroadcaster.sendBroadcast(context, id);
	}
}
