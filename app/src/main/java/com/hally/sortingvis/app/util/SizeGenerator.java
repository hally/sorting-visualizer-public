package com.hally.sortingvis.app.util;

/**
 * Class calculates and stores sizes for each element of {@code com.hally.sortingvis.app
 * .NumberContainer} based on minimum and maximum sizes from dimens.xml file and number of sorting
 * elements.
 *
 * @author Kateryna Levshova
 * @date 17.04.2015
 */
public class SizeGenerator
{
	public static final String EXCEPTION_ERROR_SIZE_ZERO = "minSize and maxSize must not be 0!";
	public static final String EXCEPTION_ERROR_SIZE_COMP = "maxSize must be grater than minSize!";
	public static final int MIN_ELEMENTS_NUMBER = 2;
	public static final String EXCEPTION_ERROR_NUMBER_LENGTH = "elementsNumber must be >= " +
			MIN_ELEMENTS_NUMBER + " !";
	private float[] _sizesArray;

	/**
	 * Calculates size for each sorting element based on minimum and maximum sizes and elements
	 * number.
	 *
	 * @param elementsNumber
	 * @param minSize
	 * @param maxSize
	 * @throws RuntimeException
	 */
	public SizeGenerator(int elementsNumber, float minSize, float maxSize) throws
			RuntimeException
	{
		_sizesArray = new float[elementsNumber];

		if (minSize == 0 || maxSize == 0)
		{
			throw new RuntimeException(EXCEPTION_ERROR_SIZE_ZERO);
		}

		if (maxSize < minSize)
		{
			throw new RuntimeException(EXCEPTION_ERROR_SIZE_COMP);
		}

		if (elementsNumber < MIN_ELEMENTS_NUMBER)
		{
			throw new RuntimeException(EXCEPTION_ERROR_NUMBER_LENGTH);
		}
		else if (elementsNumber > MIN_ELEMENTS_NUMBER)
		{
			float interval = (maxSize - minSize) / elementsNumber;
			int length = _sizesArray.length;

			for (int i = 0; i < length; i++)
			{
				_sizesArray[i] = minSize + interval * i;
			}
		}
		else
		{
			_sizesArray[0] = minSize;
			_sizesArray[1] = maxSize;
		}
	}

	/**
	 * Returns rounded up size of sorting element view
	 *
	 * @param sortingElementValue
	 * @return
	 */
	public int getSize(int sortingElementValue)
	{
		return (int) Math.ceil(_sizesArray[sortingElementValue]);
	}
}
