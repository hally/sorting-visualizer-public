package com.hally.sortingvis.app.message;

import android.content.Context;
import android.os.Bundle;

/**
 * @author Kateryna Levshova
 * @date 27.04.2015
 */
public interface ISender
{
	public void sendBroadcastBundle(Context context, String id, Bundle params);

	public void sendBroadcast(Context context, String id);
}
