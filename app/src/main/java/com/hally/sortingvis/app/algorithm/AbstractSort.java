package com.hally.sortingvis.app.algorithm;

import android.content.Context;

import com.hally.sortingvis.app.R;
import com.hally.sortingvis.app.model.AnimationManager;
import com.hally.sortingvis.app.util.TraceUtil;

import java.util.Arrays;

/**
 * Abstract class for all sorting algorithms
 *
 * @author Kateryna Levshova
 * @date 23.04.2015
 */
public abstract class AbstractSort
{
	public static final String ERROR_ARRAY_DEFINITION = "Source array is not defined properly!";
	public static final String CLASS_NAME = AbstractSort.class.getSimpleName();
	protected int[] resultArray;
	protected AnimationManager animationManager;
	protected Context context;

	public AbstractSort(Context co)
	{
		animationManager = AnimationManager.getInstance(co);
		context = co;
	}

	public int[] getResultArray()
	{
		return resultArray;
	}

	/**
	 * Initiates animations and performs sorting algorithm
	 *
	 * @param srcArray
	 * @throws RuntimeException
	 */
	public void sort(int[] srcArray) throws RuntimeException
	{
		if (srcArray == null || srcArray.length < 2)
		{
			throw new RuntimeException(ERROR_ARRAY_DEFINITION);
		}

		if (animationManager.isAnimationRunning)
		{
			animationManager.showToast(R.string.toast_wait);
		}
		else
		{
			if (animationManager.isNewSortingStarted)
			{
				resultArray = perform(srcArray, 0, srcArray.length - 1);
				animationManager.updateOutputText(R.string.txt_sorting_complete);
				animationManager.cloneAnimationQueue();
			}
			else
			{
				animationManager.playAgain();
			}

			animationManager.releaseNextQueueElement();
		}
	}

	/**
	 * Performes sorting algorithm
	 *
	 * @param srcArray
	 * @param start
	 * @param end
	 * @return
	 */
	protected abstract int[] perform(int[] srcArray, int start, int end);

	/**
	 * Swaps two elements if they are not equal in source array
	 *
	 * @param srcArray
	 * @param lowerIndex
	 * @param higherIndex
	 */
	protected void swapNumbers(int[] srcArray, int lowerIndex, int higherIndex)
	{
		TraceUtil.logD(CLASS_NAME, "swapNumbers", "with lowerIndex= " + lowerIndex + " and " +
				"higherIndex= " + higherIndex + " in " + Arrays.toString(srcArray));

		TraceUtil.logD(CLASS_NAME, "swapNumbers", "swap for " + srcArray[lowerIndex] +
				" and " + srcArray[higherIndex]);

		if (lowerIndex != higherIndex)
		{
			int temp = srcArray[lowerIndex];
			srcArray[lowerIndex] = srcArray[higherIndex];
			srcArray[higherIndex] = temp;
			TraceUtil.logD(CLASS_NAME, "swapNumbers", "swap performed!");
			animationManager.swapElements(lowerIndex, higherIndex);
		}
		else
		{
			animationManager.updateOutputText(R.string.txt_do_not_swap);
			TraceUtil.logD(CLASS_NAME, "swapNumbers", "DO not swap since elements are equal!");
		}
	}
}
