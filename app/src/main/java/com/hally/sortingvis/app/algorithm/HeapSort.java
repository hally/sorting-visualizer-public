package com.hally.sortingvis.app.algorithm;

import android.content.Context;

import com.hally.sortingvis.app.R;
import com.hally.sortingvis.app.model.Constants;
import com.hally.sortingvis.app.util.TraceUtil;

import java.util.Arrays;
import java.util.NoSuchElementException;

/**
 * @author Kateryna Levshova
 * @date 23.04.2015
 */
public class HeapSort extends AbstractSort
{
	public static final String CLASS_NAME = HeapSort.class.getSimpleName();
	private int[] _data;
	private int _size;

	public HeapSort(Context context)
	{
		super(context);
	}

	@Override
	public void sort(int[] srcArray) throws RuntimeException
	{
		super.sort(srcArray);
		TraceUtil.logD(CLASS_NAME, "-------SORTING RESULT = ", Arrays.toString(srcArray));
	}

	@Override
	protected int[] perform(int[] srcArray, int start, int end)
	{
		TraceUtil.logD(CLASS_NAME, "perform", Arrays.toString(srcArray));
		createMaxHeap(srcArray);

		for (int i = this._size - 1; i >= 0; i--)
		{
			int nextMax = removeNext();
			TraceUtil.logD(CLASS_NAME, "perform", "Remove next max=" + nextMax);
			// We do this only because we know
			// from the way the heap works that the end of the array
			// can be used to store our sorted result in place.
			_data[i] = nextMax;
		}

		return srcArray;
	}

	private int removeNext() throws NoSuchElementException
	{
		if (_size == 0)
		{
			throw new NoSuchElementException();
		}

		int next = _data[0];
		_data[0] = _data[--_size];
		TraceUtil.logD(CLASS_NAME, "removeNext", "");
		heapify(0);
		return next;
	}

	private void createMaxHeap(int srcArray[])
	{
		animationManager.updateOutputText(R.string.txt_create_heap);
		_data = srcArray;
		_size = srcArray.length;

		TraceUtil.logD(CLASS_NAME, "createMaxHeap", Arrays.toString(_data));

		for (int i = _size / 2 - 1; i >= 0; i--)
		{
			heapify(i);
		}
	}

	private void heapify(int i)
	{
		TraceUtil.logD(CLASS_NAME, "heapify", "i= " + i);
		animationManager.updateOutputTextWithFormat(R.string.txt_heapify, _data[i], i);

		// You might as well start with this optimistic assumption
		int largestElementIndex = i;
		animationManager.updateOutputTextWithFormat(R.string.txt_heapify_assumption, _data[i], i);
		animationManager.updateSortingElementUI(i);

		// The optimistic assumption leads to nice parallelism between the
		// left-child and right-child cases.
		int l = leftChild(i);
		int r = rightChild(i);

		if (l < _size && _data[l] > _data[largestElementIndex])
		{
			largestElementIndex = l;
			updateLargest(Constants.LEFT_STR, l);
		}

		if (r < _size && _data[r] > _data[largestElementIndex])
		{
			largestElementIndex = r;
			updateLargest(Constants.RIGHT_STR, r);
		}

		// If heap consistency was locally violated
		if (largestElementIndex != i)
		{
			animationManager.updateOutputText(R.string.txt_heap_violated);
			swapNumbers(_data, i, largestElementIndex);

			// Recursively heapify the affected sub-tree
			animationManager.updateOutputText(R.string.txt_heapify_recursively);
			heapify(largestElementIndex);
		}
	}

	private void updateLargest(String paramStr, int largestIndex)
	{
		animationManager.updateOutputTextWithFormat(R.string.txt_mark_index, paramStr,
				_data[largestIndex], largestIndex);
		animationManager.markIndex(largestIndex);
		animationManager.updateOutputTextWithFormat(R.string.txt_heapify_largest, paramStr,
				_data[largestIndex], largestIndex);
		animationManager.updateSortingElementUI(largestIndex);
	}

	/**
	 * Returns left index of a zero based array
	 *
	 * @param i
	 * @return
	 */
	private int leftChild(int i)
	{
		return 2 * i + 1;
	}

	/**
	 * Returns right index of a zero based array
	 *
	 * @param i
	 * @return
	 */
	private int rightChild(int i)
	{
		return 2 * i + 2;
	}
}
