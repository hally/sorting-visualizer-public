package com.hally.sortingvis.app.model;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.Gravity;
import android.widget.Toast;

import com.hally.sortingvis.app.R;
import com.hally.sortingvis.app.message.Command;
import com.hally.sortingvis.app.message.IReceiver;
import com.hally.sortingvis.app.message.ISender;
import com.hally.sortingvis.app.message.MessageBroadcaster;
import com.hally.sortingvis.app.message.MessageConstants;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * @author Kateryna Levshova
 * @date 28.04.2015
 */
public class AnimationManager implements ISender, IReceiver
{
	public static final String CLASS_NAME = AnimationManager.class.getSimpleName();
	public static boolean IS_NORMAL_MODE = true; // false - test mode
	private static AnimationManager _animationManagerInst;
	private static boolean _isInstantiationAllowed = false;
	public boolean isNewSortingStarted = true;
	public boolean isAnimationRunning = false;
	private boolean isNotLocked = true;

	protected Context _context;
	private LinkedList<Command> _animationQueue;
	private LinkedList<Command> _cloneQueue;
	private BroadcastReceiver _onMessageReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			parseMessage(intent.getAction(), intent.getExtras());
		}
	};

	private AnimationManager(Context context)
	{
		if (!_isInstantiationAllowed)
		{
			String errorMessage =
					"You cannot use Constructor to create an instance of this class. " +
							"Instead of it please use AnimationManager.getInstance().";

			throw new IllegalArgumentException(errorMessage);
		}

		_context = context;

		Constants.LEFT_STR = _context.getResources().getString(R.string.param_left);
		Constants.RIGHT_STR = _context.getResources().getString(R.string.param_right);

		if(IS_NORMAL_MODE)
		{
			resetDefault();
			registerAllMessagesReceiver();
		}
	}

	public static AnimationManager getInstance(Context context)
	{
		if (_animationManagerInst == null)
		{
			_isInstantiationAllowed = true;
			_animationManagerInst = new AnimationManager(context);
			_isInstantiationAllowed = false;
		}
		return _animationManagerInst;
	}

	private void resetDefault()
	{
		_animationQueue = new LinkedList<Command>();
		_cloneQueue = new LinkedList<Command>();
		isNewSortingStarted = true;
		isAnimationRunning = false;
		isNotLocked = true;
	}

	public void playAgain()
	{
		if(IS_NORMAL_MODE)
		{
			_animationQueue = (LinkedList) _cloneQueue.clone();
			sendBroadcast(_context, MessageConstants.RESTART_ANIMATION);
		}
	}

	public void showToast(int textId)
	{
		if(IS_NORMAL_MODE)
		{
			Toast toast = Toast.makeText(_context.getApplicationContext(), textId,
					Toast.LENGTH_SHORT);

			final TypedArray styledAttributes =
					_context.getApplicationContext().getTheme().obtainStyledAttributes(
							new int[]{android.R.attr.actionBarSize});
			int actionBarHeight = (int) styledAttributes.getDimension(0, 0);

			toast.setGravity(Gravity.TOP, 0, actionBarHeight);
			toast.show();
		}
	}

	public void cloneAnimationQueue()
	{
		_cloneQueue = (LinkedList) _animationQueue.clone();
	}

	public void startMoveIndexAnimation(int fromElement, int toElement, boolean isLower)
	{
		if(IS_NORMAL_MODE)
		{
			//show text first
			Resources res = _context.getResources();
			String param1 =
					(isLower) ? res.getString(R.string.param_lower) : res.getString(R.string.param_higher);
			updateOutputTextWithFormat(R.string.txt_move_index_to, param1, toElement);

			//start animation
			Bundle bundle = new Bundle();
			bundle.putInt(Constants.INDEX_FROM, fromElement);
			bundle.putInt(Constants.INDEX_TO, toElement);

			Command command = new Command(MessageConstants.MOVE_INDEX, bundle);
			_animationQueue.add(command);
		}
	}

	public void markIndexes(int stringId, int lowerIndex, int higherIndex)
	{
		if(IS_NORMAL_MODE)
		{
			//show text first
			updateOutputTextWithFormat(stringId, lowerIndex, higherIndex);

			//start animation
			Bundle bundle = new Bundle();
			bundle.putInt(Constants.LOWER_INDEX, lowerIndex);
			bundle.putInt(Constants.HIGHER_INDEX, higherIndex);

			Command command = new Command(MessageConstants.MARK_INDEXES, bundle);
			_animationQueue.add(command);
		}
	}

	public void markIndex(int index)
	{
		if (IS_NORMAL_MODE)
		{
			Bundle bundle = new Bundle();
			bundle.putInt(Constants.INDEX, index);

			Command command = new Command(MessageConstants.MARK_INDEX, bundle);
			_animationQueue.add(command);
		}
	}

	public void updateSortingElementUI(int index)
	{
		if(IS_NORMAL_MODE)
		{
			//start animation
			Bundle bundle = new Bundle();
			bundle.putInt(Constants.PIVOT_INDEX, index);

			Command command = new Command(MessageConstants.MARK_PIVOT, bundle);
			_animationQueue.add(command);
		}
	}

	private void updateOutputText(String text)
	{
		if(IS_NORMAL_MODE)
		{
			Bundle bundle = new Bundle();
			bundle.putString(Constants.TEXT, text);

			Command command = new Command(MessageConstants.UPDATE_OUTPUT, bundle);
			_animationQueue.add(command);
		}
	}

	public void updateOutputTextWithFormat(int stringId, Object... args)
	{
		if(IS_NORMAL_MODE)
		{
			Resources res = _context.getResources();
			String text = String.format(res.getString(stringId), args);
			updateOutputText(text);
		}
	}

	public void updateOutputText(int stringId)
	{
		if(IS_NORMAL_MODE)
		{
			Resources res = _context.getResources();
			String text = res.getString(stringId);
			updateOutputText(text);
		}
	}

	public void swapElements(int lowerIndex, int higherIndex)
	{
		if(IS_NORMAL_MODE)
		{
			//show text first
			updateOutputTextWithFormat(R.string.txt_swap_elements, lowerIndex, higherIndex);

			//start animation
			Bundle bundle = new Bundle();
			bundle.putInt(Constants.LOWER_INDEX, lowerIndex);
			bundle.putInt(Constants.HIGHER_INDEX, higherIndex);

			Command command = new Command(MessageConstants.SWAP_ELEMENTS, bundle);
			_animationQueue.add(command);
		}
	}

	public void releaseNextQueueElement()
	{
		if(IS_NORMAL_MODE)
		{
			if (isNotLocked)
			{
				Iterator iterator = _animationQueue.iterator();

				if (iterator.hasNext())
				{
					isAnimationRunning = true;
					isNewSortingStarted = false;
					Command cmd = _animationQueue.poll();
					sendBroadcastBundle(_context, cmd.name, cmd.bundle);
				}
				else
				{
					if (!isNewSortingStarted)
					{
						sendBroadcast(_context, MessageConstants.ANIMATION_SET_FINISHED);
					}
					isAnimationRunning = false;
				}
			}
		}
	}

	@Override
	public void sendBroadcastBundle(Context context, String id, Bundle params)
	{
		MessageBroadcaster.sendBroadcastBundle(context, id, params);
	}

	@Override
	public void sendBroadcast(Context context, String id)
	{
		MessageBroadcaster.sendBroadcast(context, id);
	}

	/**
	 * Call it in onCreate
	 */
	@Override
	public void registerAllMessagesReceiver()
	{
		registerOnMessageReceiver(MessageConstants.RELEASE_NEXT_ANIMATION);
		registerOnMessageReceiver(MessageConstants.ON_ACTION_RANDOMIZE);
		registerOnMessageReceiver(MessageConstants.ON_CLICK);
		registerOnMessageReceiver(MessageConstants.UNREGISTER_LISTENERS);
	}

	/**
	 * Call it in onDestroy
	 */
	@Override
	public void unregisterAllMessagesReceiver()
	{
		LocalBroadcastManager.getInstance(_context).unregisterReceiver(_onMessageReceiver);
	}

	/**
	 * Generally called inside of <code>registerAllMessagesReceiver()</code>
	 *
	 * @param messageId
	 */
	@Override
	public void registerOnMessageReceiver(String messageId)
	{
		LocalBroadcastManager.getInstance(_context).registerReceiver(_onMessageReceiver,
				new IntentFilter(messageId));
	}

	protected void parseMessage(String messageId, Bundle bundle)
	{
		switch (messageId)
		{
			case MessageConstants.RELEASE_NEXT_ANIMATION:
			{
				releaseNextQueueElement();
				break;
			}
			case MessageConstants.ON_ACTION_RANDOMIZE:
			case MessageConstants.UNREGISTER_LISTENERS:
			{
				resetDefault();
				break;
			}
			case MessageConstants.ON_CLICK:
			{
				if(isAnimationRunning)
				{
					if(isNotLocked)
					{
						isNotLocked = false;
						showToast(R.string.toast_paused);

						Bundle params = new Bundle();
						String text = _context.getResources().getString(R.string.toast_paused);
						params.putString(Constants.TEXT, text);
						sendBroadcastBundle(_context, MessageConstants.UPDATE_OUTPUT, params);
					}
					else
					{
						isNotLocked = true;
						showToast(R.string.toast_resumed);
						releaseNextQueueElement();
					}
				}
				break;
			}
		}
	}
}
