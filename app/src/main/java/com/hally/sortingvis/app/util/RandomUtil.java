package com.hally.sortingvis.app.util;

import java.util.Arrays;
import java.util.Random;

/**
 * Generates zero based array with random values without duplication
 *
 * @author Kateryna Levshova
 * @date 16.04.2015
 */
public class RandomUtil
{
	public static final String CLASS_NAME = RandomUtil.class.getSimpleName();

	private static int[] _sourceArray;

	public RandomUtil()
	{
	}

	/**
	 * Gets length of the source array
	 *
	 * @return
	 */
	public static int getValuesArrayLength()
	{
		return _sourceArray.length;
	}

	public static void printValuesArray()
	{
		TraceUtil.logD(CLASS_NAME, "printValuesArray", Arrays.toString(_sourceArray));
	}

	/**
	 * Creates array with values from 0 to <code> elementsNumber-1 </code>
	 *
	 * @param elementsNumber
	 */
	public static void initSourceArray(int elementsNumber)
	{
		_sourceArray = new int[elementsNumber];

		for (int i = 0; i < elementsNumber; i++)
		{
			_sourceArray[i] = i;
		}

		TraceUtil.logD(CLASS_NAME, "initSourceArray", Arrays.toString(_sourceArray));
	}

	/**
	 * Gets zero based array without values repetition
	 *
	 * @return
	 */
	public static int[] getValuesArray()
	{
		return _sourceArray;
	}

	/**
	 * Swaps elements of pre initialized array based on random index. It guaranties that source
	 * array will not contain repeated elements
	 */
	public static void shuffleArray()
	{
		Random random = new Random();
		int length = _sourceArray.length;

		for (int i = length - 1; i > 0; i--)
		{
			int index = random.nextInt(i + 1);
			// Simple swap
			int tmpElement = _sourceArray[index];
			_sourceArray[index] = _sourceArray[i];
			_sourceArray[i] = tmpElement;
		}

		//_sourceArray = new int[]{3, 7, 4, 0, 9, 6, 1, 5, 8, 2};

		TraceUtil.logD(CLASS_NAME, "shuffleArray", Arrays.toString(_sourceArray));
	}

	/**
	 * Generates zero based array with random sequence of elements without element's value
	 * repetition Example: 9, 8, 4, 3, 5, 7, 1, 0, 2, 6
	 *
	 * @param elementsNumber
	 */
	public static void generateRandomValues(int elementsNumber)
	{
		initSourceArray(elementsNumber);
		shuffleArray();
	}
}
