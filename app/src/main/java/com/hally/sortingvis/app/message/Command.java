package com.hally.sortingvis.app.message;

import android.os.Bundle;

/**
 * @author Kateryna Levshova
 * @date 17.06.2015
 */
public class Command
{
	public String name;
	public Bundle bundle;

	public Command(String nameStr, Bundle bundleCmd)
	{
		name = nameStr;
		bundle = bundleCmd;
	}
}
