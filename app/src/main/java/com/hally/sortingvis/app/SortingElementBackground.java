package com.hally.sortingvis.app;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * @author Kateryna Levshova
 * @date 16.04.2015
 */
public class SortingElementBackground extends ImageView
{

	public SortingElementBackground(Context context)
	{
		super(context);
	}

	public SortingElementBackground(Context context, AttributeSet attrs, int defStyleAttr,
									int defStyleRes)
	{
		super(context, attrs, defStyleAttr, defStyleRes);
	}

	public SortingElementBackground(Context context, AttributeSet attrs, int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
	}

	public SortingElementBackground(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
	{
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}
}
