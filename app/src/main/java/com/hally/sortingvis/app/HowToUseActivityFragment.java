package com.hally.sortingvis.app;

import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Locale;

/**
 * A placeholder fragment containing a simple view.
 */
public class HowToUseActivityFragment extends Fragment
{
	private final static String IMAGE_PLACEHOLDER = "IMAGE_PLACEHOLDER";
	private TextView _txtStep1;
	private TextView _txtStep2;
	private TextView _txtStep3;
	private TextView _txtStep4;
	private TextView _txtStep5;

	public HowToUseActivityFragment()
	{
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.fragment_how_to_use, container, false);
		_txtStep1 = (TextView) view.findViewById(R.id.txtStep1);
		_txtStep2 = (TextView) view.findViewById(R.id.txtStep2);
		_txtStep3 = (TextView) view.findViewById(R.id.txtStep3);
		_txtStep4 = (TextView) view.findViewById(R.id.txtStep4);
		_txtStep5 = (TextView) view.findViewById(R.id.txtStep5);

		setCustomFont();
		initText();

		return view;
	}

	private void setCustomFont()
	{
		String language = Locale.getDefault().getLanguage();
		if(!language.equals("ru"))
		{
			Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/journal.ttf");
			_txtStep1.setTypeface(tf);
			_txtStep2.setTypeface(tf);
			_txtStep3.setTypeface(tf);
			_txtStep4.setTypeface(tf);
			_txtStep5.setTypeface(tf);
		}
	}

	private void initText()
	{
		_txtStep1.setText(Html.fromHtml(getString(R.string.step1)));
		_txtStep2.setText(Html.fromHtml(getString(R.string.step2)));
		_txtStep3.setText(Html.fromHtml(getString(R.string.step3)));

		// 4th step
		_txtStep4.setText(Html.fromHtml(getString(R.string.step4)));

		//5th step
		Spanned spanned = Html.fromHtml(getString(R.string.step5), new Html.ImageGetter()
		{
			@Override
			public Drawable getDrawable(String source)
			{
				Drawable randomizeBtnImage = getResources().getDrawable(R.mipmap.ic_randomize);
				randomizeBtnImage
						.setBounds(0, 0, _txtStep1.getLineHeight(), _txtStep1.getLineHeight());
				return randomizeBtnImage;
			}
		}, null);

		_txtStep5.setText(spanned);
	}
}
